/* This is a generated file, don't edit */

#define NUM_APPLETS 42
#define KNOWN_APPNAME_OFFSETS 4

const uint16_t applet_nameofs[] ALIGN2 = {
51,
112,
162,
};

const char applet_names[] ALIGN1 = ""
"bash" "\0"
"cat" "\0"
"chat" "\0"
"chmod" "\0"
"chown" "\0"
"chroot" "\0"
"clear" "\0"
"cp" "\0"
"cut" "\0"
"date" "\0"
"dd" "\0"
"du" "\0"
"echo" "\0"
"fdisk" "\0"
"free" "\0"
"head" "\0"
"hostname" "\0"
"id" "\0"
"ifconfig" "\0"
"init" "\0"
"install" "\0"
"ip" "\0"
"kill" "\0"
"killall" "\0"
"less" "\0"
"ln" "\0"
"ls" "\0"
"lspci" "\0"
"lsusb" "\0"
"mount" "\0"
"ping" "\0"
"pkill" "\0"
"poweroff" "\0"
"ps" "\0"
"readahead" "\0"
"reboot" "\0"
"sh" "\0"
"sleep" "\0"
"time" "\0"
"umount" "\0"
"uname" "\0"
"vi" "\0"
;

#define APPLET_NO_bash 0
#define APPLET_NO_cat 1
#define APPLET_NO_chat 2
#define APPLET_NO_chmod 3
#define APPLET_NO_chown 4
#define APPLET_NO_chroot 5
#define APPLET_NO_clear 6
#define APPLET_NO_cp 7
#define APPLET_NO_cut 8
#define APPLET_NO_date 9
#define APPLET_NO_dd 10
#define APPLET_NO_du 11
#define APPLET_NO_echo 12
#define APPLET_NO_fdisk 13
#define APPLET_NO_free 14
#define APPLET_NO_head 15
#define APPLET_NO_hostname 16
#define APPLET_NO_id 17
#define APPLET_NO_ifconfig 18
#define APPLET_NO_init 19
#define APPLET_NO_install 20
#define APPLET_NO_ip 21
#define APPLET_NO_kill 22
#define APPLET_NO_killall 23
#define APPLET_NO_less 24
#define APPLET_NO_ln 25
#define APPLET_NO_ls 26
#define APPLET_NO_lspci 27
#define APPLET_NO_lsusb 28
#define APPLET_NO_mount 29
#define APPLET_NO_ping 30
#define APPLET_NO_pkill 31
#define APPLET_NO_poweroff 32
#define APPLET_NO_ps 33
#define APPLET_NO_readahead 34
#define APPLET_NO_reboot 35
#define APPLET_NO_sh 36
#define APPLET_NO_sleep 37
#define APPLET_NO_time 38
#define APPLET_NO_umount 39
#define APPLET_NO_uname 40
#define APPLET_NO_vi 41

#ifndef SKIP_applet_main
int (*const applet_main[])(int argc, char **argv) = {
ash_main,
cat_main,
chat_main,
chmod_main,
chown_main,
chroot_main,
clear_main,
cp_main,
cut_main,
date_main,
dd_main,
du_main,
echo_main,
fdisk_main,
free_main,
head_main,
hostname_main,
id_main,
ifconfig_main,
init_main,
install_main,
ip_main,
kill_main,
kill_main,
less_main,
ln_main,
ls_main,
lspci_main,
lsusb_main,
mount_main,
ping_main,
pgrep_main,
halt_main,
ps_main,
readahead_main,
halt_main,
ash_main,
sleep_main,
time_main,
umount_main,
uname_main,
vi_main,
};
#endif

